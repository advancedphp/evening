<?php include 'include/header.php';?>
<?php 
	$referer = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : "";
	if(!isset($_GET['id']) || $_GET['id'] == "" || $_GET['id'] == 0){
		if($referer != ""){
			header("location: ".$referer);
			exit;
		}
		header('location: index');
		exit;
	}

	$product_id = (int)$product->sanitize($_GET['id']);
	$product_info = $product->getProductById($product_id);

	/*debugger($product_info, true);*/
?>
<script src='assets/js/okzoom.js'></script>
  <script>
    $(function(){
      $('#example').okzoom({
        width: 200,
        height: 200,
        border: "1px solid black",
        shadow: "0 0 5px #000"
      });
    });
  </script>
  <style>
  	.thumb-responsive {
	    max-width: 63px;
	    float: left;
	    margin-right: 5px;
	    border: 1px solid #eee;
	    margin-top: 5px;
	    padding: 3px;
	    cursor: pointer;
	}
  </style>
<!-- header -->
<?php include 'include/top-nav.php';?>
<!-- //header -->
<!-- banner -->
	<div class="banner">
		<?php include 'include/sidebar.php';?>
		<div class="w3l_banner_nav_right">
			<?php if($product_info['product_info'] == ""){
					echo '<p class="alert alert-success">Sorry! The product you specified does not exists.</p>';
				} else {
			?>
			<div class="agileinfo_single">
				<h5><?php echo $product_info['product_info']['title'];?></h5>
				<div class="col-md-4">
					<div class="main-image agileinfo_single_left">
						<?php 
							if($product_info['images'] == "") {
								$first_image = IMAGES_URL."noImage.png";
							} else {
								$first_image = UPLOADS."product/".$product_info['images'][0]['image_name'];
							}
						?>
							<img id="example" src="<?php echo $first_image;?>" alt=" " class="img-responsive" />
					</div>
					
					<div class="thumbs">
						<?php 
							if($product_info['images'] != ""){
								foreach($product_info['images'] as $images){
						?>
							<img src="<?php echo UPLOADS.'product/'.$images['image_name'];?>" alt=" " class="img-responsive thumb-responsive" onclick="showImage(this)" />
						<?php }

						}
						?>
					</div>
				</div>
				<div class="col-md-8 agileinfo_single_right">
					<div class="rating1">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5" disabled>
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4" disabled >
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" disabled >
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2" disabled checked>
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1" disabled>
							<label for="rating1">1</label>
						</span>
					</div>
					<div class="w3agile_description">
						<h4>Description :</h4>
						<?php echo html_entity_decode($product_info['product_info']['description']); ?>
					</div>
					<div class="snipcart-item block">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
							<h4>Rs. <?php  echo ($product_info['product_info']['price'] - $product_info['product_info']['discount']);?> 
								<?php if($product_info['product_info']['discount'] > 0){ ?>
								<span>Rs. <?php echo $product_info['product_info']['price']; ?></span>
								<?php } ?>
							</h4>
						</div>
						<div class="snipcart-details agileinfo_single_right_details">
							<form action="#" method="post">
								<fieldset>
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" />
									<input type="hidden" name="business" value=" " />
									<input type="hidden" name="item_name" value="<?php echo $product_info['product_info']['title'];?>" />
									<input type="hidden" name="amount" value="<?php echo $product_info['product_info']['price'];?>" />
									<input type="hidden" name="discount_amount" value="<?php echo $product_info['product_info']['discount'];?>" />
									<input type="hidden" name="currency_code" value="INR" />
									<input type="hidden" name="return" value=" " />
									<input type="hidden" name="cancel_return" value=" " />
									<input type="submit" name="submit" value="Add to cart" class="button" />
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<?php } ?>
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->
<!-- brands -->
	<div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_popular">
		<div class="container">
			<h3>Realted Products</h3>
				<div class="w3ls_w3l_banner_nav_right_grid1">
					<div class="col-md-3 w3ls_w3l_banner_left">
						<div class="hover14 column">
						<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
							<div class="agile_top_brand_left_grid_pos">
								<img src="assets/images/offer.png" alt=" " class="img-responsive" />
							</div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">
											<a href="single.html"><img src="assets/images/5.png" alt=" " class="img-responsive" /></a>
											<p>knorr instant soup (100 gm)</p>
											<h4>$3.00 <span>$5.00</span></h4>
										</div>
										<div class="snipcart-details">
											<form action="#" method="post">
												<fieldset>
													<input type="hidden" name="cmd" value="_cart" />
													<input type="hidden" name="add" value="1" />
													<input type="hidden" name="business" value=" " />
													<input type="hidden" name="item_name" value="knorr instant soup" />
													<input type="hidden" name="amount" value="3.00" />
													<input type="hidden" name="discount_amount" value="1.00" />
													<input type="hidden" name="currency_code" value="USD" />
													<input type="hidden" name="return" value=" " />
													<input type="hidden" name="cancel_return" value=" " />
													<input type="submit" name="submit" value="Add to cart" class="button" />
												</fieldset>
											</form>
										</div>
									</div>
								</figure>
							</div>
						</div>
						</div>
					</div>
					<div class="col-md-3 w3ls_w3l_banner_left">
						<div class="hover14 column">
						<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
							<div class="agile_top_brand_left_grid_pos">
								<img src="assets/images/offer.png" alt=" " class="img-responsive" />
							</div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">
											<a href="single.html"><img src="assets/images/6.png" alt=" " class="img-responsive" /></a>
											<p>chings noodles (75 gm)</p>
											<h4>$5.00 <span>$8.00</span></h4>
										</div>
										<div class="snipcart-details">
											<form action="#" method="post">
												<fieldset>
													<input type="hidden" name="cmd" value="_cart" />
													<input type="hidden" name="add" value="1" />
													<input type="hidden" name="business" value=" " />
													<input type="hidden" name="item_name" value="chings noodles" />
													<input type="hidden" name="amount" value="5.00" />
													<input type="hidden" name="discount_amount" value="1.00" />
													<input type="hidden" name="currency_code" value="USD" />
													<input type="hidden" name="return" value=" " />
													<input type="hidden" name="cancel_return" value=" " />
													<input type="submit" name="submit" value="Add to cart" class="button" />
												</fieldset>
											</form>
										</div>
									</div>
								</figure>
							</div>
						</div>
						</div>
					</div>
					<div class="col-md-3 w3ls_w3l_banner_left">
						<div class="hover14 column">
						<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
							<div class="agile_top_brand_left_grid_pos">
								<img src="assets/images/offer.png" alt=" " class="img-responsive" />
							</div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">
											<a href="single.html"><img src="assets/images/7.png" alt=" " class="img-responsive" /></a>
											<p>lahsun sev (150 gm)</p>
											<h4>$3.00 <span>$5.00</span></h4>
										</div>
										<div class="snipcart-details">
											<form action="#" method="post">
												<fieldset>
													<input type="hidden" name="cmd" value="_cart" />
													<input type="hidden" name="add" value="1" />
													<input type="hidden" name="business" value=" " />
													<input type="hidden" name="item_name" value="lahsun sev" />
													<input type="hidden" name="amount" value="3.00" />
													<input type="hidden" name="discount_amount" value="1.00" />
													<input type="hidden" name="currency_code" value="USD" />
													<input type="hidden" name="return" value=" " />
													<input type="hidden" name="cancel_return" value=" " />
													<input type="submit" name="submit" value="Add to cart" class="button" />
												</fieldset>
											</form>
										</div>
									</div>
								</figure>
							</div>
						</div>
						</div>
					</div>
					<div class="col-md-3 w3ls_w3l_banner_left">
						<div class="hover14 column">
						<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
							<div class="agile_top_brand_left_grid_pos">
								<img src="assets/images/offer.png" alt=" " class="img-responsive" />
							</div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">
											<a href="single.html"><img src="assets/images/8.png" alt=" " class="img-responsive" /></a>
											<p>premium bake rusk (300 gm)</p>
											<h4>$5.00 <span>$7.00</span></h4>
										</div>
										<div class="snipcart-details">
											<form action="#" method="post">
												<fieldset>
													<input type="hidden" name="cmd" value="_cart" />
													<input type="hidden" name="add" value="1" />
													<input type="hidden" name="business" value=" " />
													<input type="hidden" name="item_name" value="premium bake rusk" />
													<input type="hidden" name="amount" value="5.00" />
													<input type="hidden" name="discount_amount" value="1.00" />
													<input type="hidden" name="currency_code" value="USD" />
													<input type="hidden" name="return" value=" " />
													<input type="hidden" name="cancel_return" value=" " />
													<input type="submit" name="submit" value="Add to cart" class="button" />
												</fieldset>
											</form>
										</div>
									</div>
								</figure>
							</div>
						</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
		</div>
	</div>
<!-- //brands -->
<?php include 'include/footer.php';?>
<script type="text/javascript">
	function showImage(elem){
		var to_change = $(elem).attr('src');
		$('#example').attr('src', to_change);
	}
</script>