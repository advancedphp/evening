<?php include 'include/header.php'; 
	
	if(!isset($_POST) || empty($_POST)){
		@header('location: index');
		exit;
	}
	$keyword = $product->sanitize($_POST['keyword']);
	$search_result = $product->searchProduct($keyword);
?>
<!-- header -->
<?php include 'include/top-nav.php'; ?>
<style type="text/css">
	.search-result{
		margin-left: 20px;
    margin-top: 20px;
    border-bottom: 1px solid #ccc;
    margin-bottom: 10px;
    margin-right: 20px;
        
	}
	.col-sm-4.thumbnail {
    max-width: 200px;
}
.product-detail h4 {
    font-size: x-large;
}
.summary {
    font-size: small;
    text-align: justify;
    margin-top: 5px;
}
p.price {
    font-weight: bold;
        margin-top: 5px;
    margin-bottom: 5px;
}
span.discount {
    margin-left: 50px;
    text-decoration: line-through;
}
.w3l_banner_nav_right {
    float: left;
    width: 61%;
}
.right-sidebar {
    border-left: 1px solid #555;
    float: left;
    padding-left: 28px;
}
</style>
<!-- banner -->
	<div class="banner">
		<?php include 'include/sidebar.php'; ?>
		<div class="w3l_banner_nav_right">
			<h4 style="margin-left: 20px">Search result</h4>
			<?php 
				if($search_result){
					foreach($search_result as $searched_data){
					?>
					<div class="row search-result">
						<div class="col-sm-4 thumbnail">
							<img src="assets/images/1.png" class="img-responsive" />
						</div>
						<div class="col-sm-8 product-detail">
							<h4>Product title</h4>
							<div class="summary">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate laborum corporis cumque beatae aut, quidem vel, harum hic veniam qui voluptas tenetur.
							</div>
							<p class="price">Rs. 2000 <span class="discount">Rs. 2500</span></p>
							<a href="product?id=1" class="btn btn-success">View Detail</a>
						</div>
					
					</div>
					<?php 	}
					} else {
				echo '<p class="alert alert-danger">Sorry! The product you searched does not exists.</p>';
				} ?>
		</div>
		<div class="right-sidebar">
			<div class="row">
				<div class="filters">
					<h4>Filters</h4>
					<h5>Price</h5>
					<form method="post" action="search">
						<input type="checkbox" name="price[]" multiple value="1000" /> <= Rs. 1000 <br />
						<input type="checkbox" name="price[]" multiple value="5000" /> Rs. 1000 - Rs. 5000 <br />
						<input type="checkbox" name="price[]" multiple value="10000" /> Rs. 5000 - Rs. 10000 <br />
						<input type="checkbox" name="price[]" multiple value="50000" /> Rs. 10000 - Rs. 50000 <br />
						<input type="checkbox" name="price[]" multiple value="100000" /> > Rs. 50000 <br />
						<input type="submit" name="submit" value="Filter" class="btn btn-primary">
					</form>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->

<?php include 'include/footer.php'; ?>