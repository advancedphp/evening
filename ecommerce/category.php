<?php
	if(!isset($_GET['cid']) || $_GET['cid'] == ""){
		header('location: index');
		exit;
	}
?>
<?php include 'include/header.php'; 
	$cat_id = $category->sanitize($_GET['cid']);
	$cat_info = $category->getCategoryInfoById($cat_id);
	$no_data = 0;
	if(!$cat_info){
		$no_data = 1;
	}
	$child_cat = $category->getChildCategoryByCatId($cat_info[0]['id']);


?>
<!-- header -->
<?php include 'include/top-nav.php'; ?>
<!-- banner -->
	<div class="banner">
		<?php include 'include/sidebar.php'; ?>
		<div class="w3l_banner_nav_right">
			
			<?php 
				if($no_data == 0){
			?>
			<div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_sub">
				<h3><?php echo $cat_info[0]['title'];?></h3>
				<?php 
					if($child_cat){
						foreach($child_cat as $children){
						?>
						<div class="w3ls_w3l_banner_nav_right_grid1">
							<h6><?php echo $children['title'];?></h6>
							<?php 
								$sub_cat_product = $product->getProductBysubCat($cat_info[0]['id'], $children['id']);
								if($sub_cat_product){
									foreach($sub_cat_product as $product_sub_cat){
									?>
										<div class="col-md-3 w3ls_w3l_banner_left">
											<div class="hover14 column">
											<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
												
												<?php 
													if($product_sub_cat['discount'] != 0 || $product_sub_cat['discount'] != ""){
												?>
												<div class="agile_top_brand_left_grid_pos">
													<img src="<?php echo IMAGES_URL;?>offer.png" alt=" " class="img-responsive" />
												</div>
												<?php 
													 }
												?>
												<div class="agile_top_brand_left_grid1">
													<figure>
														<div class="snipcart-item block">
															<div class="snipcart-thumb">
																<a href="product?id=<?php echo $product_sub_cat['id'];?>">
																	<?php
																		if(file_exists("uploads/product/".$product_sub_cat['image_name']) && $product_sub_cat['image_name'] != "" ){
																			$thumbnail = UPLOADS."product/".$product_sub_cat['image_name'];
																		} else {
																			$thumbnail = IMAGES_URL."noImage.png";
																		}
																	?>
																	<img src="<?php echo $thumbnail;?>" alt=" " class="img-responsive" />
																</a>
																<p><?php echo $product_sub_cat['title'];?></p>
																<h4>
																	<?php
																		$price = $product_sub_cat['price'];
																		$discount = $product_sub_cat['discount'];
																	?>
																	NPR. <?php echo ($discount != "" || $discount != 0) ? ($price-$discount) : $price;?>
																	<?php if($discount > 0){ ?>
																	<span>NPR. <?php echo $price;?></span>
																	<?php } ?>
																</h4>
															</div>
															<div class="snipcart-details">
																<form action="#" method="post">
																	<fieldset>
																		<input type="hidden" name="cmd" value="_cart" />
																		<input type="hidden" name="add" value="1" />
																		<input type="hidden" name="business" value=" " />
																		<input type="hidden" name="item_name" value="<?php echo $product_sub_cat['title'];?>" />
																		<input type="hidden" name="amount" value="<?php echo $product_sub_cat['price'];?>" />
																		<input type="hidden" name="discount_amount" value="<?php echo $product_sub_cat['discount'];?>" />
																		<input type="hidden" name="currency_code" value="INR" />
																		<input type="hidden" name="return" value=" " />
																		<input type="hidden" name="cancel_return" value=" " />
																		<input type="submit" name="submit" value="Add to cart" class="button" />
																	</fieldset>
																</form>
															</div>
														</div>
													</figure>
												</div>
											</div>
											</div>
										</div>

									<?php
									}
								} else {
									echo "<p>Sorry! There are no any products in this category.</p>";
								}
							?>
							<div class="clearfix"> </div>
						</div>

						<?php
						}
					} else {
						$cat_product = $product->getCategoryProduct($cat_info[0]['id']);
						if($cat_product){
							foreach($cat_product as $category_product){
								?>
								<div class="col-md-3 w3ls_w3l_banner_left" style="max-height: 333px; min-height: 333px">
									<div class="hover14 column">
									<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid" style="height: 333px !important; ">
										
										<?php 
											if($category_product['discount'] != 0 || $category_product['discount'] != ""){
										?>
										<div class="agile_top_brand_left_grid_pos">
											<img src="<?php echo IMAGES_URL;?>offer.png" alt=" " class="img-responsive" />
										</div>
										<?php 
											 }
										?>
										<div class="agile_top_brand_left_grid1" style="height: 314px;">
											<figure>
												<div class="snipcart-item block">
													<div class="snipcart-thumb" style="min-height: 230px;">
														<div class="thumb-wrap" style="min-height: 140px;">
															<a href="product?id=<?php echo $category_product['id'];?>">
																<?php
																	if(file_exists("uploads/product/".$category_product['image_name']) && $category_product['image_name'] != "" ){
																		$thumbnail = UPLOADS."product/".$category_product['image_name'];
																	} else {
																		$thumbnail = IMAGES_URL."noImage.png";
																	}
																?>
																<img src="<?php echo $thumbnail;?>" alt=" " class="img-responsive" />
															</a>
														</div>
														<p><?php echo $category_product['title'];?></p>
														<h4>
															<?php
																$price = $category_product['price'];
																$discount = $category_product['discount'];
															?>
															NPR. <?php echo ($discount != "" || $discount != 0) ? ($price-$discount) : $price;?>
															<?php if($discount > 0){ ?>
															<span>NPR. <?php echo $price;?></span>
															<?php } ?>
														</h4>
													</div>
													<div class="snipcart-details">
														<form action="#" method="post">
															<fieldset>
																<input type="hidden" name="cmd" value="_cart" />
																<input type="hidden" name="add" value="1" />
																<input type="hidden" name="business" value=" " />
																<input type="hidden" name="item_name" value="<?php echo $category_product['title'];?>" />
																<input type="hidden" name="amount" value="<?php echo $category_product['price'];?>" />
																<input type="hidden" name="discount_amount" value="<?php echo $category_product['discount'];?>" />
																<input type="hidden" name="currency_code" value="INR" />
																<input type="hidden" name="return" value=" " />
																<input type="hidden" name="cancel_return" value=" " />
																<input type="submit" name="submit" value="Add to cart" class="button" />
															</fieldset>
														</form>
													</div>
												</div>
											</figure>
										</div>
									</div>
									</div>
								</div>
								<?php
							}
						} else {
							echo "<p class='alert alert-info'>Sorry! There are ano any product in this category.</p>";
						}

					}
				?>
			</div>

			<?php } else {
				echo '<p>Sorry! Category informtaion does not exist.</p>';
				} ?>
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->

<?php include 'include/footer.php'; ?>