<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-23 15:34:08
 * @Organization: Knockout System Pvt. Ltd.
 */
function debugger($array, $isDie = false){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
	if($isDie){
		exit;
	}
}

function getCurrentPage(){
	$php_self = explode("/",$_SERVER['PHP_SELF']);
	$current_page = array_pop($php_self);
	return $current_page;
}

function getFileExtension($fileName){
	$ext = pathinfo($fileName, PATHINFO_EXTENSION);
	return $ext;
}
?>
