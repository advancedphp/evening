<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-06 17:18:49
 * @Organization: Knockout System Pvt. Ltd.
 */
?>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="dashboard">Hamrostore.com</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['full_name'];?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <?php 
        $cur_page = getCurrentPage();
    ?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="<?php echo ($cur_page == 'dashboard.php' || $cur_page == 'dashboard') ? 'active' : '';?>">
                <a href="dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
			<li class="<?php echo ($cur_page == 'banner-add.php' || $cur_page == 'banner-list.php') ? 'active' : '';?>">
                <a href="javascript:0;" data-toggle="collapse" data-target="#banner" class="" aria-expanded="false">
                	<i class="fa fa-fw fa-image"></i> Banner Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="banner" class="collapse" aria-expanded="false">
                    <li>
                        <a href="banner-add">Add Banner</a>
                    </li>
                    <li>
                        <a href="#">List Banner</a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($cur_page == 'ads-add.php' || $cur_page == 'ads-list.php') ? 'active' : '';?>">
                <a href="javascript:0;" data-toggle="collapse" data-target="#ads" class="" aria-expanded="false">
                	<i class="fa fa-fw fa-dollar"></i> Ads Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="ads" class="collapse" aria-expanded="false">
                    <li>
                        <a href="#">Add Ads</a>
                    </li>
                    <li>
                        <a href="#">List Ads</a>
                    </li>
                </ul>
            </li>
			<li class="<?php echo ($cur_page == 'category-add.php' || $cur_page == 'category-list.php') ? 'active' : '';?>">
                <a href="javascript:0;" data-toggle="collapse" data-target="#category" class="" aria-expanded="false">
                	<i class="fa fa-fw fa-list"></i> Category Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="category" class="collapse" aria-expanded="false">
                    <li>
                        <a href="category-add">Add Category</a>
                    </li>
                    <li>
                        <a href="#">List Category</a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($cur_page == 'product-add.php' || $cur_page == 'product-list.php') ? 'active' : '';?>">
                <a href="javascript:0;" data-toggle="collapse" data-target="#product" class="" aria-expanded="false">
                	<i class="fa fa-fw fa-suitcase"></i> Product Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="product" class="collapse" aria-expanded="false">
                    <li>
                        <a href="product-add">Add Product</a>
                    </li>
                    <li>
                        <a href="#">List Product</a>
                    </li>
                </ul>
            </li>
            <li  class="<?php echo ($cur_page == 'page-add.php' || $cur_page == 'page-list.php') ? 'active' : '';?>">
                <a href="javascript:0;" data-toggle="collapse" data-target="#page" class="" aria-expanded="false">
                	<i class="fa fa-fw fa-file"></i> Page Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="page" class="collapse" aria-expanded="false">
                    <li>
                        <a href="#">Add Page</a>
                    </li>
                    <li>
                        <a href="#">List Page</a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($cur_page == 'user-add.php' || $cur_page == 'user-list.php') ? 'active' : '';?>">
                <a href="javascript:0;" data-toggle="collapse" data-target="#user" class="" aria-expanded="false">
                	<i class="fa fa-fw fa-user"></i> User Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="page" class="collapse" aria-expanded="false">
                    <li>
                        <a href="#">Add User</a>
                    </li>
                    <li>
                        <a href="#">List User</a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($cur_page == 'subscriber.php') ? 'active' : '';?>">
                <a href="#"><i class="fa fa-fw fa-users"></i> Subscibers</a>
            </li>

            <li class="<?php echo ($cur_page == 'order.php') ? 'active' : '';?>">
                <a href="#"><i class="fa fa-fw fa-shopping-cart"></i> Orders</a>
            </li>

        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>