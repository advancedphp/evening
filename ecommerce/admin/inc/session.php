<?php
    session_start();
    if((isset($_SESSION['id']) && $_SESSION['id'] == "") || !isset($_SESSION['id']) || $_SESSION['role_id'] == 3 ){
        $_SESSION['error'] = "Please login first";
        header('location: index.php');
        exit;
    }
?>