<?php include 'inc/session.php'; ?>
<?php include 'inc/config.php';?>
<?php include 'inc/header.php';?>
<?php include 'inc/generalFunctions.php';?>
<?php
	$act = "add";

?>
    <div id="wrapper">
        <?php include 'inc/navigation.php';?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?php include 'inc/notification.php';?>
                        <h1 class="page-header">
                            Banner <?php echo ucfirst($act);?>
                        </h1>
						<form method="post" action="controller/banner.php" enctype="multipart/form-data" class="form form-horizontal">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Banner Title: </label>
								<div class="col-sm-9">
									<input type="text" name="banner_title" id="banner_title" requrired class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Banner Link: </label>
								<div class="col-sm-9">
									<input type="text" name="banner_link" id="banner_link" requrired class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Banner Status: </label>
								<div class="col-sm-9">
									<select name="banner_status" id="banner_status" required="" class="form-control">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="" class="control-label col-sm-3">Banner Image</label>
								<div class="col-sm-9">
									<input type="file" name="banner_image" <?php echo ($act === "add") ?'required':'';?> id="banner_image" onchange="showThumbnail(this)"/>
									<img src="assets/images/noImage.png" class="img-responsive thumbnail pull-right" style="max-width: 250px; " id="thumbnail" />
								</div>
							</div>

							<div class="form-group">
								<input type="submit" name="submit" value="<?php echo ucfirst($act);?> Banner" class="btn btn-success">
							</div>

						</form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12"> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php';?>
<script type="text/javascript">
	function showThumbnail(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('#thumbnail').attr('src',e.target.result);
			}
			reader.readAsDataURL(input.files[0]);

		}
	}
</script>