<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-07-06 15:43:51
 * @Organization: Knockout System Pvt. Ltd.
 */
include '../inc/session.php';
include '../inc/config.php';
include '../inc/generalFunctions.php';
include '../Model/database.php';
include '../Model/product.php';

$product = new Product();

if(isset($_POST['submit']) && $_POST['submit'] != ""){
	$data = array();
	$data['title'] = $product->sanitize($_POST['Product_title']);
	$data['summary'] = $product->sanitize($_POST['summary']);
	$data['description'] = htmlentities($product->sanitize($_POST['description']));
	$data['cat_id'] = $product->sanitize($_POST['category_id']);
	$data['child_cat_id'] = $product->sanitize($_POST['child_cat_id']);
	$data['vendor'] = $product->sanitize($_POST['vendor']);
	$data['price'] = $product->sanitize($_POST['price']);
	$data['discount'] = $product->sanitize($_POST['discount']);
	$data['stock'] = $product->sanitize($_POST['stock']);
	$data['status'] = $product->sanitize($_POST['status']);
	$data['delivery_charge'] = $product->sanitize($_POST['delivery_charge']);
	$data['added_by'] = $_SESSION['id'];
/*debugger($_FILES,true);*/
	$insert_id = $product->addProduct($data);

	if($insert_id){
		if(isset($_FILES['product_image'])){
			$upload_path = UPLOADS_DIR;

			if(!is_dir($upload_path)){
				mkdir($upload_path);
			}

			$upload_dir = $upload_path."/product";
			if(!is_dir($upload_dir)){
				mkdir($upload_dir);
			}

			for($i=0; $i <count($_FILES['product_image']['name']); $i++){
				$tmp_image = array();
				$ext = getFileExtension($_FILES['product_image']['name'][$i]);
				if(in_array($ext, explode(',',ALLOWED_EXTENSIONS))){
					$date = strtotime(date('Y-m-d h:i:s'));
					$fileName = "product-".$date."-".rand(0,999).".".$ext;
					$success = move_uploaded_file($_FILES['product_image']['tmp_name'][$i], $upload_dir."/".$fileName);
					$tmp_image['product_id'] = $insert_id;
					$tmp_image['image_name'] = $fileName;
				}
				$images[] = $tmp_image;
			}
			$images_uploaded = $product->addProductImages($images);

			if($images_uploaded){
				$_SESSION['success'] = "Product added successfully";
			} else {
				$_SESSION['info'] = "Product added but images could not be added at this moment.";
			}
			@header('location: ../product-add.php');
			exit;
		}
	} else {
		$_SESSION['error'] = "Sorry! Product could not be added at this moment.";
		@header('location: ../product=add.php');
		exit;
	}
	/*debugger($_POST);
	debugger($_FILES,true);*/
} else {
	$_SESSION['warning'] = "Invalid Access";
	@header('location: ../product=add.php');
	exit;
}