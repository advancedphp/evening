<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-29 15:17:50
 * @Organization: Knockout System Pvt. Ltd.
 */
include '../inc/session.php';
include '../inc/config.php';
include '../inc/generalFunctions.php';
include '../Model/database.php';
include '../Model/banner.php';
$banner = new Banner();

if(isset($_POST['submit']) && $_POST['submit'] != ""){

	$data = array();
	$data['banner_title'] = $banner->sanitize($_POST['banner_title']);
	$data['banner_link'] = $banner->sanitize($_POST['banner_link']);
	$data['banner_status'] = $banner->sanitize($_POST['banner_status']);


	if(isset($_FILES['banner_image']) && $_FILES['banner_image']['error'] == 0){
		$ext = getFileExtension($_FILES['banner_image']['name']);
		$fileName = "Banner-".strtotime(date('Y-m-d h:i:s'))."-".rand(0,9999).".".$ext;

		if(in_array($ext, explode(",",ALLOWED_EXTENSIONS))){
			$upload_dir = UPLOADS_DIR;
			$upload_path = $upload_dir."/banner";

			if(!is_dir($upload_dir)){
				mkdir($upload_dir);
			}

			if(!is_dir($upload_path)){
				mkdir($upload_path);
			}

			$success = move_uploaded_file($_FILES['banner_image']['tmp_name'], $upload_path."/".$fileName);
			if(!$success){
				$fileName = "";
			}
		}
	} else {
		$fileName = "";
	}
	$data['banner_image'] = $fileName;

	$data_id = $banner->addBanner($data);
	if($data_id){
		$_SESSION['success'] = "Banner Added successfully";
		@header('location: banner-list');
		exit;
	} else {
		if(file_exists(UPLOADS_DIR."/banner/".$data['banner_image'])){
			unlink(UPLOADS_DIR."/banner/".$data['banner_image']);
		}
		$_SESSION['error'] = "Sorry! There was problem while adding banner";
		@header('location: banner-list');
		exit;
	}
	/*debugger($_POST);*/
	/*debugger($data_id, true);*/
} else {
	$_SESSION['info'] = "Invalid access.";
	@header('location: banner-list');
	exit;
}