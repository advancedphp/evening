<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-23 15:51:46
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
include '../inc/config.php';
include '../inc/generalFunctions.php';
include '../Model/database.php';
include '../Model/user.php';

$user = new User();

if(isset($_POST['submit']) && $_POST['submit'] != ""){
	$user_name = $user->sanitize($_POST['username']);
	$password = sha1($_POST['password']);

	$user_info = $user->getUserByUsername($user_name);
	if($user_info){
		if($password == $user_info[0]['password']){
			$_SESSION['id'] = $user_info[0]['id'];
			$_SESSION['full_name'] = $user_info[0]['full_name'];
			$_SESSION['email_address'] = $user_info[0]['email_address'];
			$_SESSION['email_address'] = $user_info[0]['email_address'];
			$_SESSION['role_id'] = $user_info[0]['role_id'];
			$_SESSION['image'] = $user_info[0]['image'];

			$update = @$user->updateLogin($user_info[0]['id']);

			$_SESSION['success'] = "Welcome to hamrostore.com admin panel";
			@header("location: ../dashboard");
			exit;
		} else {
			$_SESSION['error'] = "Password does not match";
			@header("location: ../");
			exit;	
		}
	} else {
		$_SESSION['error'] = "Username does not exist";
		@header("location: ../");
		exit;
	}

	debugger($user_info,true);

} else {
	$_SESSION['error'] = "Invalid Access";
	@header('location: ../');
	exit;
}
?>
