<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-07-02 16:10:14
 * @Organization: Knockout System Pvt. Ltd.
 */
include '../inc/session.php';
include '../inc/config.php';
include '../inc/generalFunctions.php';
include '../Model/database.php';
include '../Model/category.php';
$category = new Category();

if(isset($_POST['submit']) && $_POST['submit'] != ""){

	/*debugger($_POST, true);*/

	$data = array();
	$data['title'] = $category->sanitize($_POST['category_title']);
	$data['status'] = $category->sanitize($_POST['category_status']);
	$data['show_in_menu'] = $category->sanitize($_POST['show_in_menu']);
	$data['is_parent'] = (isset($_POST['is_parent'])) ? $category->sanitize($_POST['is_parent']) : 0;
	$data['parent_id'] = (isset($_POST['parent_id']) && $_POST['is_parent'] != 1 ) ? $category->sanitize($_POST['parent_id']) : 0;

	if($data['is_parent'] == 1){
		$data['parent_id'] = 0;
	}
	$data_id = $category->addCategory($data);
	if($data_id){ 
		$_SESSION['success'] = "Category Added successfully";
		@header('location: category-list');
		exit;
	} else {
		$_SESSION['error'] = "Sorry! There was problem while adding Category";
		@header('location: category-list');
		exit;
	}
	/*debugger($_POST);*/
	/*debugger($data_id, true);*/
} else if(isset($_POST['act']) && $_POST['act'] == "get-child-cat" && isset($_POST['category_id']) && $_POST['category_id'] != ""){
	$cat_id = $category->sanitize($_POST['category_id']);
	$child_cat = $category->getChildByParentId($cat_id);
	if($child_cat){
		echo json_encode($child_cat);
		exit;
	} else {
		echo 0;
		exit;
	}
} 

else {
	$_SESSION['info'] = "Invalid access.";
	@header('location: category-list');
	exit;
}