<?php include 'inc/session.php'; ?>
<?php include 'inc/config.php';?>
<?php include 'inc/header.php';?>
<?php include 'inc/generalFunctions.php';?>
<?php include 'Model/database.php';?>
<?php include 'Model/category.php';?>
<?php
	$category = new Category();
	$act = "add";

?>
    <div id="wrapper">
        <?php include 'inc/navigation.php';?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?php include 'inc/notification.php';?>
                        <h1 class="page-header">
                            Category <?php echo ucfirst($act);?>
                        </h1>
						<form method="post" action="controller/category.php" enctype="multipart/form-data" class="form form-horizontal">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Category Title: </label>
								<div class="col-sm-9">
									<input type="text" name="category_title" id="category_title" requrired class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Category Status: </label>
								<div class="col-sm-9">
									<select name="category_status" id="category_status" required="" class="form-control">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Show In Menu:</label>
								<div class="col-sm-9">
									<input type="checkbox" name="show_in_menu" value="1" id="show_in_menu" />
								</div>
							</div>

							

							<?php
								$parent_cats = $category->getAllParent();
								if($parent_cats){
							?>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Is this parent Category?:</label>
								<div class="col-sm-9">
									<input type="checkbox" name="is_parent" value="1" id="is_parent" /> Yes
								</div>
							</div>
							<div class="form-group" id="is_parent_div">
								<label for="" class="col-sm-3 control-label">Parent Category</label>
								<div class="col-sm-9">
									<select name="parent_id" id="parent_id" class="form-control">
										<?php 
											foreach($parent_cats as $cats){
											?>
											<option value="<?php echo $cats['id'];?>"><?php echo $cats['title'];?></option>
											<?php
											}
										?>
									</select>
								</div>
							</div>
							<?php } ?>
							
							<div class="form-group">
								<input type="submit" name="submit" value="<?php echo ucfirst($act);?> Category" class="btn btn-success">
							</div>

						</form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12"> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php';?>
<script type="text/javascript">
	$('#is_parent').on('click',function(e){
		var is_checked = $('#is_parent').prop('checked');
		if(is_checked){
			$('#is_parent_div').hide();
		} else {
			$('#is_parent_div').show();
		}
	});
</script>