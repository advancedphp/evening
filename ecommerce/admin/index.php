<?php 
    session_start();
    if(isset($_SESSION['id']) || $_SESSION['id'] != "" || (isset($_SESSION['role_id']) && $_SESSION['role_id'] != 3 ) ){
        $_SESSION['success'] = "Welcome to Admin panel of hamrostore.com";
        header('location: dashboard');
        exit;
    }
    include 'inc/config.php';
    include 'inc/generalFunctions.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Admin || Hamrostore.com</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo CSS_URL;?>bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo CSS_URL;?>sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo FONT_AWESOME_URL;?>css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.assets/js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper col-md-10">


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <?php include 'inc/notification.php';?>
                	<form method="post" name="login" action="controller/login.php">
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" class="form-control" id="username" required />
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" class="form-control" id="username" required />
						</div>
						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-primary" id="username" required />
						</div>
                	</form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo JS_URL;?>jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo JS_URL;?>bootstrap.min.js"></script>
</body>

</html>
