<?php
    include 'inc/session.php';
    include 'inc/config.php';
    include 'inc/generalFunctions.php';
    include 'Model/database.php';
    include 'Model/category.php';

    
    $category = new Category();
    
    $act = "add";
    $pageTitle = "Product ".ucfirst($act)." || Supermarket Store";
    include 'inc/header.php';
    


?>
    <div id="wrapper">

        <?php include 'inc/navigation.php';?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo ucfirst($act);?> Product
                        </h1>
                        <form class="form form-horizontal" method="post" action="controller/product.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Title:</label>
                                <div class="col-md-9 col-sm-9">
                                    <input type="text" required name="Product_title" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Summary</label>
                                <div class="col-md-9 col-sm-9">
                                    <textarea name="summary" id="summary" cols="30" rows="7" class="form-control" style="resize: vertical;" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Description</label>
                                <div class="col-md-9 col-sm-9">
                                    <textarea name="description" id="description" cols="30" rows="7" class="form-control" style="resize: vertical;"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Status:</label>
                                <div class="col-md-9 col-sm-9">
                                    <select name="status" id="status" class="form-control" required>
                                        <option value="1" <?php echo (isset($product_data[0]['Product_status']) && $product_data[0]['Product_status'] == 1) ? 'selected' : '';?> >Active</option>
                                        <option value="0" <?php echo (isset($product_data[0]['Product_status']) && $product_data[0]['Product_status'] == 0) ? 'selected' : '';?> >Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Category:</label>
                                <div class="col-sm-9 col-md-9">
                                    <select name="category_id" id="category_id" required class="form-control">
                                        <option value="">--Select Category--</option>
                                        <?php

                                            $parent_cat = $category->getAllParent();
                                            if($parent_cat){
                                                foreach($parent_cat as $key=>$cat_info){
                                                ?>
                                                    <option value="<?php echo $cat_info['id'];?>"><?php echo $cat_info['title'];?></option>
                                                <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                    <span class="alert alert-danger hidden" id="parent_error"></span>
                                </div>
                            </div>
                            
                                
                            <div class="form-group hidden" id="child_cat">
                                <label for="" class="col-sm-3 control-label">Child Category:</label>
                                <div class="col-sm-9">
                                    <select name="child_cat_id" id="child_cat_id" class="form-control">
                                        
                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Price:</label>
                                <div class="col-md-9 col-sm-9">
                                    <input type="number" step="0.5" name="price" min="1" required id="price" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Discount:</label>
                                <div class="col-md-9 col-sm-9">
                                    <input type="number" step="0.5" name="discount" min="0" id="discount"  class="form-control" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Delivery Cost:</label>
                                <div class="col-md-9 col-sm-9">
                                    <input type="number" step="0.5" name="delivery_charge" id="delivery_charge"  class="form-control" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Stock:</label>
                                <div class="col-md-9 col-sm-9">
                                    <input type="number" step="1" name="stock" min="1" id="stock"  class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Vendor:</label>
                                <div class="col-md-9 col-sm-9">
                                    <input type="text" name="vendor" id="vendor"  class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Image:</label>
                                <div class="col-md-9 col-sm-9">
                                    <input type="file" name="product_image[]" accept="image/*" multiple <?php echo ($act == 'add') ? 'required' : '';?> />
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="Product_id" value="<?php echo (isset($product_data[0]['id'])) ? $product_data[0]['id'] : '';?>" />
                                <input class="btn btn-success" name="submit" type="submit"  value="<?php echo ucfirst($act);?> Product" id="submit" />
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

                
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php
$scripts = '<script type="text/javascript" src="'.JS_URL.'plugins/tinymce/tinymce.min.js"></script>';
include 'inc/footer.php'; ?>

<script type="text/javascript">
    tinymce.init({
        selector: '#description',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
          ],
          toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
          toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
          image_advtab: true,
    });

    $('#category_id').on('change', function(e){
        var cat_id = $('#category_id').val();
        $.post('controller/category.php',{category_id: cat_id, act: 'get-child-cat'},function(data){
            if(data != 0){
                var parsed_data = $.parseJSON(data);
                var html = '';
                $.each(parsed_data, function(index,value){
                    html += '<option value="'+value['id']+'">'+value['title']+'</option>';
                });
                $('#child_cat_id').html(html);
                $('#child_cat').removeClass('hidden');
            } else {
                $('#child_cat').addClass('hidden');
            }
        });
    });
</script>