<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-23 15:26:19
 * @Organization: Knockout System Pvt. Ltd.
 */
abstract class Database{
	public $conn;
	private $db;

	private $fields;
	private $table;
	private $where;
	private $groupby;
	private $orderby;
	private $limit;
	private $join;

	private $sql;
	private $stmt;
	private $result;
	private $field_value;

	public function __construct(){
		$this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS) or die('Error Establishing Database Connection');
		$this->db = $this->conn->select_db(DB_NAME) or die($this->conn->error);
	}

	public function sanitize($str){
		/*trim();*/
		return $this->conn->real_escape_string($str);
	}

	protected function groupBy($condition){
		$this->groupby = " GROUP BY ".$condition;
	}

	protected function orderBy($condition){
		$this->orderby = " ORDER BY ".$condition;
	}

	protected function limit($index, $count){
		$this->limit = " LIMIT ".$index.", ".$count;
	}

	protected function join($clause){
		$this->join = $clause;
	}

	protected function select($isDie = false){
		/*SELECT <fields> FROM <table> <JOIN STATEMENT> WHERE <Where Clause> GROUP BY <group by clause> ORDER BY <order by CLAUSE> LIMIT <index>, <limit>*/

		$this->sql = "SELECT ";
		$this->sql .= $this->fields;
		$this->sql .= " FROM ".$this->table;
		/*JOIN STATEMENT*/
		if(isset($this->join) && $this->join != ""){ 
			$this->sql .= $this->join;
		}

		if(isset($this->where) && $this->where != ""){ 
			$this->sql .= $this->where;
		}
		/*Group By*/
		if(isset($this->groupby) && $this->groupby != ""){ 
			$this->sql .= $this->groupby;
		}

		/*ORDER BY*/
		if(isset($this->orderby) && $this->orderby != ""){ 
			$this->sql .= $this->orderby;
		}

		/*LIMIT */
		if(isset($this->limit) && $this->limit != ""){ 
			$this->sql .= $this->limit;
		}
		
		if($isDie){
			echo $this->sql;
			exit;
		}

		$this->stmt = $this->conn->prepare($this->sql) or die($this->conn->error);
		$this->stmt->execute() or die($this->conn->error);
		$this->result = $this->stmt->get_result();

		if($this->result->num_rows <= 0){
			return false;
		} else {
			$data = array();
			while($row = $this->result->fetch_assoc()){
				$data[] = $row;
			}
			return $data;
		}

	}

	protected function fields($fields = '*'){
		if(is_array($fields)){
			$this->fields = implode(",",$fields);
		} else {
			$this->fields = $fields;
		}

	}

	protected function table($table){
		$this->table = $table;
	}

	protected function where($condition){
		$this->where = " WHERE ".$condition;
	}

	private function prepareInsertUpdateFields($fields, $values){
		$temp = array();
		foreach($fields as $key => $val){
			$str = "";
			$str = $val." = ";
			if(is_string($values[$key]) && $values[$key] != "now()" ){
				$str .= "'".$values[$key]."'";
			} else {
				$str .= $values[$key];
			}
			$temp[] = $str;
		}
		$this->field_value = implode(", ",$temp);
	}

	protected function update($fields, $values, $isDie = false){
		$this->prepareInsertUpdateFields($fields, $values);
		$this->sql = "UPDATE ";
		$this->sql .= $this->table;
		$this->sql .= " SET ";
		$this->sql .= $this->field_value;
		$this->sql .= $this->where;

		if($isDie){
			die($this->sql);
		} else {
			$this->stmt = $this->conn->prepare($this->sql);
			return $this->stmt->execute();
		}


	}

	protected function insert($fields, $values, $isDie = false){
		$this->prepareInsertUpdateFields($fields, $values);
		$this->sql = "INSERT INTO ";
		$this->sql .= $this->table;
		$this->sql .= " SET ";
		$this->sql .= $this->field_value;

		if($isDie){
			die($this->sql);
		} else {
			$this->stmt = $this->conn->prepare($this->sql) or die($this->conn->error);
			$result = $this->stmt->execute();
			if($result){
				return $this->conn->insert_id;
			} else {
				return false;
			}
		}


	}
}
?>
