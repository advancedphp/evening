<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-23 15:58:39
 * @Organization: Knockout System Pvt. Ltd.
 */

class User extends Database{
	public function getUserByUserName($username){
		$this->table('users');
		$this->fields();
		$this->where(" username = '".$username."' AND status = 1");
		return $this->select();
	}

	public function updateLogin($user_id){
		$this->table('users');
		$this->where(' id = '.$user_id);
		$fields = array('login_ip', 'login_date');
		$values = array($_SERVER['REMOTE_ADDR'], 'now()');
		return $this->update($fields, $values);
	}	
}
?>
