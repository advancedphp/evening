<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-29 15:23:01
 * @Organization: Knockout System Pvt. Ltd.
 */
class Banner extends Database{
	public function addBanner($data){
		$this->table('banners');
		$fields = array('banner_title','banner_link','banner_status', 'banner_image','added_by');
		$values = array($data['banner_title'], $data['banner_link'], $data['banner_status'], $data['banner_image'], $_SESSION['id']);
		$insert_id = $this->insert($fields, $values);
		return $insert_id;
	}

	public function getHomeBanner($limit){
		$this->table('banners');
		$this->fields();
		$this->where(' banner_status = 1 ');
		$this->orderBy(' id DESC ');
		$this->limit(0, $limit);
		$data = $this->select();
		return $data;
	}
}
