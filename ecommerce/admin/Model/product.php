<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-07-06 15:47:27
 * @Organization: Knockout System Pvt. Ltd.
 */
class Product extends Database{
	public function addProduct($data){
		
		$this->table('products ');
		$fields = array_keys($data);
		$value = array_values($data);
		/*debugger($fields);
		debugger($value, true);*/
		$insert_id = $this->insert($fields, $value);
		return $insert_id;
	}

	public function addProductImages($data){
		$this->table('product_images');
		$fields = array_keys($data[0]);
		foreach($data as $image_data){
			$values = array_values($image_data);
			$success = $this->insert($fields, $values);
		}
		return $success;
	}

	public function getProductBysubCat($cat_id, $child_cat_id){
		$this->table('products ');
		$this->fields('products.id, products.title, products.price, products.discount, product_images.image_name');
		$this->join(" LEFT JOIN product_images ON products.id = product_images.product_id ");
		$this->where(' products.cat_id = '.$cat_id." AND products.child_cat_id = ".$child_cat_id." AND products.status = 1");
		$this->groupBy(' products.id ');
		$this->orderBy(' products.id DESC');
		$this->limit(0, 4);
		$data = $this->select();
		return $data;
	}

	public function getCategoryProduct($cat_id){
		$this->table('products ');
		$this->fields('products.id, products.title, products.price, products.discount, product_images.image_name');
		$this->join(" LEFT JOIN product_images ON products.id = product_images.product_id ");
		$this->where(' products.cat_id = '.$cat_id." AND products.status = 1");
		$this->groupBy(' products.id ');
		$this->orderBy(' products.id DESC');
		$this->limit(0, 20);
		$data = $this->select();
		return $data;
	}

	public function getProductById($id){
		$this->table('products ');
		$this->fields();
		$this->where(' products.id = '.$id." AND products.status = 1 ");
		$data = $this->select();

		$product_images = $this->getProductImageByProductId($id);
		$datas['product_info'] = $data[0];
		$datas['images'] = $product_images;
		$datas['related_product'] = $this->getRelatedProduct($id, $data[0]['cat_id']);

		return $datas;
	}

	public function getProductImageByProductId($pid){
		$this->table('product_images ');
		$this->fields();
		$this->where(' product_id = '.$pid);
		$data = $this->select();
		return $data;
	}

	public function getRelatedProduct($id, $cat_id){
		$this->table('products ');
		$this->fields('products.id, products.title, products.price, products.discount, product_images.image_name');
		$this->join(" LEFT JOIN product_images ON products.id = product_images.product_id ");
		$this->where(' products.cat_id = '.$cat_id." AND products.status = 1 AND products.id != ".$id);
		$this->groupBy(' products.id ');
		$this->orderBy(' products.id DESC');
		$this->limit(0, 4);
		$data = $this->select();
		return $data;
	}

	public function searchProduct($keyword){
		$this->table('products ');
		$this->fields('products.id, products.title, products.price, products.discount, product_images.image_name');
		$this->join(" LEFT JOIN product_images ON products.id = product_images.product_id ");
		$this->where(' products.title LIKE "%'.$keyword.'%" AND products.status = 1 ' );
		$this->groupBy(' products.id ');
		$this->orderBy(' products.id DESC');
		$this->limit(0, 20);
		$data = $this->select();
		return $data;
	}
}