<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-07-02 16:02:17
 * @Organization: Knockout System Pvt. Ltd.
 */
class Category extends Database{
	public function getAllParent(){
		$this->table('categories');
		$this->fields();
		$this->where(' is_parent = 1 AND parent_id = 0');
		$data = $this->select();

		return $data;
	}
	
	public function addCategory($data){
		$this->table('categories');
		$fields = array('title','status','is_parent', 'parent_id', 'show_in_menu', 'added_by');
		$values = array($data['title'], $data['status'], $data['is_parent'],$data['parent_id'], $data['show_in_menu'], $_SESSION['id']);
		$insert_id = $this->insert($fields, $values);
		return $insert_id;
	}

	public function getChildByParentId($parent_id){
		$this->table('categories');
		$this->fields();
		$this->where(' is_parent = 0 AND parent_id = '.$parent_id);
		$data = $this->select();

		return $data;
	}

	public function getCategoryForMenu(){
		$this->table('categories');
		$this->fields();
		$this->where(' is_parent = 1 AND parent_id = 0 AND status = 1 AND show_in_menu = 1 ');
		$data = $this->select();

		return $data;
	}

	public function getChildCategoryByCatId($parent_id){
		$this->table('categories');
		$this->fields();
		$this->where(' is_parent = 0 AND parent_id = '.$parent_id.' AND status = 1');
		$data = $this->select();

		return $data;
	}

	public function getCategoryInfoById($cat_id){
		$this->table('categories');
		$this->fields();
		$this->where(' id = '.$cat_id);
		$data = $this->select();

		return $data;	
	}

}
