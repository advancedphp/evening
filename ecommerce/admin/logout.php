<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-28 15:28:22
 * @Organization: Knockout System Pvt. Ltd.
 */
	session_start();
	session_destroy();
	@header('location: index');
	exit;
?>
