<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-23 15:20:26
 * @Organization: Knockout System Pvt. Ltd.
 */
const DB_HOST = "localhost";
const DB_USER = "root";
const DB_PASS = "";
const DB_NAME = "hamrostore";

$url = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'];
define('SITE_URL', $url."/");
const ADMIN_URL = SITE_URL."admin/";

const ASSETS_URL = SITE_URL."assets/";
const CSS_URL = ASSETS_URL."css/";
const JS_URL = ASSETS_URL."js/";
const IMAGES_URL = ASSETS_URL."images/";
const FONTS_URL = ASSETS_URL."fonts/";
const FONT_AWESOME_URL = ASSETS_URL."font-awesome/";

const UPLOADS = SITE_URL."uploads/";
const PAGE_TITLE = "Hamrostore.com";

?>
